bl_info = {
    "name": "Compositing_templates",
    "author": "Sebastián Pastrana et Valentina Arango",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "location": "View3D > Toolself",
    "description": "allows you to select some templates for the render",
   
}
#Hello Valentin! alors, on à travaillé en dehors du fork, dans de bout code differents,
#on s'est divisés le travail par sections; Sebastian à travaillé dans le script qui importe les nodes et en définir les templates
# Valentina s'est ocupée de gerer les operators et rendre le travail de Sebastian accesible parmi le buttons, en soit gérer tout le côté UI
import bpy
import mathutils

class CompositingMainPanel(bpy.types.Panel):
    bl_label = "Compositing Library"
    bl_idname = "COMPOSITING_PT_MAINPANEL"
    bl_space_type = 'NODE_EDITOR'
    bl_region_type = 'UI'
    bl_category = "Compositing templates"

    def draw(self, context):
        layout = self.layout
        
        row = layout.row()
        row.label(text= 'Select a template to be added in your compositing')
        
        
#Create Sub Panel for templates

class SubPanelColors(bpy.types.Panel):
    bl_label = "Color Templates"
    bl_idname = "COMPOSITING_PT_COLORS"
    bl_space_type = 'NODE_EDITOR'
    bl_region_type = 'UI'
    bl_category = 'Compositing templates'
    bl_parent_id = 'COMPOSITING_PT_MAINPANEL'
    bl_options = {'DEFAULT_CLOSED'}
      
    def draw(self, context):
        layout = self.layout
        layout.scale_y = 1.1   
        
        row = layout.row()
        row.label(text= "Select a Basic Color Template.") 
        
        row = layout.row()
        row = layout.row()
        row.operator('template.sepia_operator', icon = 'KEYTYPE_KEYFRAME_VEC')
        row.operator('template.cyanotype_operator', icon= 'KEYTYPE_BREAKDOWN_VEC' )
        row.operator('template.bnw_operator', icon= 'HANDLETYPE_FREE_VEC')
        row.operator('template.chromatic_aberration', icon= 'KEYTYPE_EXTREME_VEC')
     
  
       
#Create Sub Panel (render button)
class SubPanelRender(bpy.types.Panel):
    bl_label = "Render"
    bl_idname = "COMPOSITING_PT_RENDER"
    bl_space_type = 'NODE_EDITOR'
    bl_region_type = 'UI'
    bl_category = 'Compositing templates'
    bl_parent_id = 'COMPOSITING_PT_MAINPANEL'
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout
        layout.scale_y = 1.1
        
        row = layout.row()
        row.label(text= "Make a render image")
        row = layout.row()
        row.operator('render.render_image')
        row = layout.row()    
        
        
        
 #we create the color templates       
            
 #sepia
        
class TEMPLATE_OT_SEPIA (bpy.types.Operator):
    bl_label = "Sepia"
    bl_idname = 'template.sepia_operator' 
    
    def execute(self, context):
        
        # Set up compositing nodes
        bpy.context.scene.use_nodes = True
        nodetree = bpy.context.scene.node_tree

        # Clear existing nodes
        for node in nodetree.nodes:
            nodetree.nodes.remove(node)

        # RenderLayer
        render_layers = nodetree.nodes.new(type='CompositorNodeRLayers')
        render_layers.location = (0, 0)

        # Color Balance
        composite_ColorBalance = nodetree.nodes.new("CompositorNodeColorBalance")
        composite_ColorBalance.location = (0,500)
        composite_ColorBalance.gamma = mathutils.Color((1.12, 0.93, 0.71))
        composite_ColorBalance.lift  = mathutils.Color((1.22, 1.19, 0.88))
        composite_ColorBalance.gain  = mathutils.Color((1.12, 0.93, 0.70))

        # Adding Compositing
        composite_node = nodetree.nodes.new(type='CompositorNodeComposite')
        composite_node.location = (0,1000)

        links = nodetree.links
        link = links.new(render_layers.outputs['Image'],composite_ColorBalance.inputs[1])
        link = links.new(composite_ColorBalance.outputs[0], composite_node.inputs['Image'])
        
        return {'FINISHED'}
    
 #blue
    
class TEMPLATE_OT_CYANOTYPE (bpy.types.Operator):
    bl_label = "cyanotype"
    bl_idname = 'template.cyanotype_operator' 
    
    def execute(self, context):
        
        # Set up compositing nodes
        bpy.context.scene.use_nodes = True
        nodetree = bpy.context.scene.node_tree

        # Clear existing nodes
        for node in nodetree.nodes:
            nodetree.nodes.remove(node)

        # RenderLayer
        render_layers = nodetree.nodes.new(type='CompositorNodeRLayers')
        render_layers.location = (0, 0)

        # Color Balance
        composite_ColorBalance = nodetree.nodes.new("CompositorNodeColorBalance")
        composite_ColorBalance.location = (0,500)
        composite_ColorBalance.gamma = mathutils.Color((0.81, 0.88, 1.08))
        composite_ColorBalance.lift  = mathutils.Color((0.85, 0.89, 1.22))
        composite_ColorBalance.gain  = mathutils.Color((0.78, 0.82, 1.14))

        # Adding Compositing
        composite_node = nodetree.nodes.new(type='CompositorNodeComposite')
        composite_node.location = (0,1000)

        links = nodetree.links
        link = links.new(render_layers.outputs['Image'],composite_ColorBalance.inputs[1])
        link = links.new(composite_ColorBalance.outputs[0], composite_node.inputs['Image'])
        
        return {'FINISHED'}
    
#Noir et Blanc
    
class TEMPLATE_OT_BNW (bpy.types.Operator):
    bl_label = "bnw"
    bl_idname = 'template.bnw_operator' 
    
    def execute(self, context):
        
        # Set up compositing nodes
        bpy.context.scene.use_nodes = True
        nodetree = bpy.context.scene.node_tree

        # Clear existing nodes
        for node in nodetree.nodes:
            nodetree.nodes.remove(node)    
            
       # Create Render Layers node
        render_layers = nodetree.nodes.new(type='CompositorNodeRLayers')
        render_layers.location = (0, 0)

        # Create Black and White node
        bw_node = nodetree.nodes.new(type='CompositorNodeRGBToBW')
        bw_node.location = (200, 0)

        # Create Composite node
        composite_node = nodetree.nodes.new(type='CompositorNodeComposite')
        composite_node.location = (400, 0)

        # Link nodes
        links = nodetree.links
        link = links.new(render_layers.outputs['Image'], bw_node.inputs[0])
        link = links.new(bw_node.outputs[0], composite_node.inputs['Image'])
        
        return {'FINISHED'}

#aberration chromatique       
        
class ChromaticAberrationOperator(bpy.types.Operator):
    bl_label = "Chromatic Aberration"
    bl_idname = "template.chromatic_aberration"
    
    def execute(self, context):
        # Set up compositing nodes
        bpy.context.scene.use_nodes = True
        nodetree = bpy.context.scene.node_tree

        # Clear existing nodes
        for node in nodetree.nodes:
            nodetree.nodes.remove(node)

        # RenderLayer
        render_layers = nodetree.nodes.new(type='CompositorNodeRLayers')
        render_layers.location = (0, 0)

        # Chromatic Aberration
        r_shift = nodetree.nodes.new(type='CompositorNodeLensdist')
        r_shift.inputs['Dispersion'].default_value = 0.53
        r_shift.inputs['Distortion'].default_value = 0.6
        
        r_shift.location = (-200, 300)
        
        g_shift = nodetree.nodes.new(type='CompositorNodeLensdist')
        g_shift.inputs['Dispersion'].default_value = 1.0
        g_shift.inputs['Distortion'].default_value = 0.6
        g_shift.location = (0, 300)
        
        b_shift = nodetree.nodes.new(type='CompositorNodeLensdist')
        b_shift.inputs['Dispersion'].default_value = -0.2
        b_shift.inputs['Distortion'].default_value = 0.6
        b_shift.location = (200, 300)

        # Mix nodes
        mix1 = nodetree.nodes.new(type='CompositorNodeMixRGB')
        mix1.blend_type = 'MULTIPLY'
        mix1.location = (0, 500)

        mix2 = nodetree.nodes.new(type='CompositorNodeMixRGB')
        mix2.blend_type = 'ADD'
        mix2.location = (200, 500)

        # Adding Compositing
        composite_node = nodetree.nodes.new(type='CompositorNodeComposite')
        composite_node.location = (0, 600)

        # Connect nodes
        links = nodetree.links
        link_render_to_rshift = links.new(render_layers.outputs['Image'], r_shift.inputs['Image'])
        link_render_to_gshift = links.new(render_layers.outputs['Image'], g_shift.inputs['Image'])
        link_render_to_bshift = links.new(render_layers.outputs['Image'], b_shift.inputs['Image'])
        
        link_r_to_mix1 = links.new(r_shift.outputs['Image'], mix1.inputs[1])
        link_g_to_mix1 = links.new(g_shift.outputs['Image'], mix1.inputs[2])
        link_b_to_mix2 = links.new(b_shift.outputs['Image'], mix2.inputs[2])

        link_mix1_to_mix2 = links.new(mix1.outputs[0], mix2.inputs[1])

        link_mix2_to_composite = links.new(mix2.outputs[0], composite_node.inputs['Image'])
        
        return {'FINISHED'}
        
  # here we did the class for the render botton
  
class RENDER_OT_RenderImage(bpy.types.Operator):
    bl_label = "Render Image"
    bl_idname = "render.render_image"        
     
    def execute(self, context):
        bpy.ops.render.render("INVOKE_DEFAULT", write_still=True)
        return {'FINISHED'}       
       
def register():
    bpy.utils.register_class(CompositingMainPanel)    
    bpy.utils.register_class(TEMPLATE_OT_SEPIA) 
    bpy.utils.register_class(TEMPLATE_OT_CYANOTYPE)
    bpy.utils.register_class(TEMPLATE_OT_BNW)
    bpy.utils.register_class(RENDER_OT_RenderImage)
    bpy.utils.register_class(SubPanelColors)
    bpy.utils.register_class(SubPanelRender)
    bpy.utils.register_class(ChromaticAberrationOperator)



def unregister():
    bpy.utils.unregister_class(CompositingMainPanel)
    bpy.utils.unregister_class(TEMPLATE_OT_SEPIA)
    bpy.utils.unregister_class(TEMPLATE_OT_CYANOTYPE)
    bpy.utils.unregister_class(TEMPLATE_OT_BNW)
    bpy.utils.unregister_class(RENDER_OT_RenderImage)
    bpy.utils.unregister_class(SubPanelColors)
    bpy.utils.unregister_class(SubPanelRender)
    bpy.utils.unregister_class(ChromaticAberrationOperator)
    


if __name__ == "__main__":
    register()
    